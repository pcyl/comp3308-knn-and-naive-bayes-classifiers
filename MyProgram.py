import sys
import os.path
import csv
from extrapackage.naivebayes import NaiveBayes
from extrapackage.knn import KNearestNeighbour
from extrapackage.crossvalidation import CrossValidation

trainingData = []
testingData = []

# Load CSV
def loadCsv(filename):
    dataset = []
    # Open csv file
    with open(filename, 'rb') as csvfile:
        reader =  csv.reader(csvfile)
        # iterate over each row of data
        for row in reader:
            record = [] # declare set
            count = 0
            # iterate over each field in the row
            for attr in row:
                try: # try to convert to float
                    val = float(row[count])
                except ValueError:
                    val = row[count]
                record.append(val)
                count += 1

            dataset.append(record) # add processed row to set
    return dataset

def main():
    '''
    First argument is the training data file
    Second argument is the testing data file
    Third argument is either using the k-nearest neighbor or the naives bayes algorithm
    '''

    ## Check if the argument length is only 4 (including the python file)
    if(len(sys.argv) == 4):
        csvTrain = sys.argv[1]
        csvTest = sys.argv[2]
        algorithm = sys.argv[3]

        ## Check to see if csv files are within the current path
        if (os.path.isfile(csvTrain) and os.path.isfile(csvTest)):
        # if (os.path.isfile(csvTrain)):
            ## Check for what algorithm to use
            trainingData = loadCsv(csvTrain)
            testingData = loadCsv(csvTest)
            ## Naives Bayes
            if (algorithm == 'NB'):

                # print "Classifier: Naive Bayes"
                NB = NaiveBayes()
                NB.trainingData = trainingData
                NB.testingData = testingData
                NB.on()
                # CV = CrossValidation()
                # CV.stratify(trainingData)

            ## K-Nearest Neighbor
            elif (algorithm[-2:] == 'NN'):

                # print "Classifier: K-Nearest Neighbor"
                if (not algorithm[0:-2]):
                    print "Please enter an integer as: kNN where k is an integer"
                else:
                    KNN = KNearestNeighbour()
                    KNN.trainingData = trainingData
                    KNN.testingData = testingData
                    KNN.kNumber = algorithm[0:-2]
                    KNN.on()
                    
            else:
                print "Please enter an algorithm for the third argument"

        else:
            print("File {0} or {1} does not exist").format(csvTrain,csvTest)

    else:
        print "Usage: python main.py <training data> <test data> <algorithm>"

if __name__ == "__main__":
    main()
