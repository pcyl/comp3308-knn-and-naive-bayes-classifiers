import math

class CrossValidation():
    def __init__(self):
        self.type = 'CrossValidation'

    def stratify(self, data):
        splitData = self.splitClass(data)
        classYes = splitData['classYes']
        classNo = splitData['classNo']

        numPerFoldYes = len(classYes)/10
        numPerFoldNo = len(classNo)/10

        stratData = []
        for fold in xrange(0,10):
            stratList = []
            for k in xrange(numPerFoldYes):
                stratList.append(classYes.pop()) # Pop to avoid repeats
            for k in xrange(numPerFoldNo):
                stratList.append(classNo.pop())
            stratData.append(stratList)

        if len(classYes) != 0:
            for k in xrange(len(classYes)):
                stratData[k].append(classYes.pop())
        if len(classNo) != 0:
            for k in xrange(len(classNo)):
                stratData[k].append(classNo.pop())

        count = 1
        with open("pima-folds.csv", 'wb') as output_file:
        	for fold in enumerate(stratData):
        		fold_str = 'fold ' + count
        		output_file.write(fold_str)
        		output_file.write('\n')

        		for instance in fold:
        			for i, attr in enumerate(instance):
        				if (i != 0):
        					output_file.write(',')
        				output_file.write(str(instance[attr]))

        			output_file.write('\n')

        		if (idx != 9):
        			output_file.write('\n')
                count += 1
        return stratData

    def ten_fold_cross(self, data, k=None):
        return

    # Seperate data into their classes: Yes and No
    def splitClass(self,dataset):
        classYes = []
        classNo = []
        for row in dataset:
            if row[-1] == 'yes':
                classYes.append(row)
            elif row[-1] == 'no':
                classNo.append(row)
        return {'classYes': classYes, 'classNo': classNo}
