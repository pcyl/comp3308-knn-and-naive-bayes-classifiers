import math
from operator import itemgetter

'''
 K nearest neighbour classifier, sort values by using a distance formula, sort them ascending order,
 then get the closest value(s) to zero depending on the k value. Then get the most frequent class
 and that particular class is the class for that particular row in the testing set
'''

class KNearestNeighbour():
    # Initialisation of the csv files and the k value
    def __init__(self):
        self.trainingData = []
        self.testingData = []
        self.kNumber = None

    def on(self):
        k = self.kNumber
        # Split the data into the corresponing classes (yes or no)
        splitTrainData = self.splitClass(self.trainingData)
        classYes = splitTrainData['classYes']
        classNo = splitTrainData['classNo']

        for test in self.testingData:
            # Initialise the list for Yes and No classes
            distanceY = []
            distanceN = []

            # Iterate through the lists
            for train in classYes:
                # Class Yes
                distanceY.append((self.getEuclideanDistance(test,train), 'yes'))
            for train in classNo:
                # Class No
                distanceN.append((self.getEuclideanDistance(test,train), 'no'))
            # Evaluate the values and get the first k values for each
            distance = sorted(distanceY + distanceN)[:int(k)]
            classYorN = {}
            # Iterate through the first k values
            for x in range(len(distance)):
                result = distance[x][-1]
                if result in classYorN:
                    classYorN[result] += 1
                else:
                    classYorN[result] = 1
            # Sort the values for the most frequent in descending order
            sortedValue = sorted(classYorN.iteritems(), key=itemgetter(1), reverse=True)
            print sortedValue[0][0]
    # Calculation of the Euclidean Distance between two data points
    def getEuclideanDistance(self,test,train):
        distance = 0
        length = len(train) - 1
        for i in range(length):
            distance += math.pow((test[i] - train[i]), 2)
        return math.sqrt(distance)

    # Split the data into two sets, such that one is 'Yes' and the other 'No'
    def splitClass(self,dataset):
        classYes = []
        classNo = []
        for row in dataset:
            if row[-1] == 'yes':
                classYes.append(row)
            elif row[-1] == 'no':
                classNo.append(row)
        return {'classYes': classYes, 'classNo': classNo}
