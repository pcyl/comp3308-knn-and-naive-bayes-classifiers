# COMP3308 - Assignment 1: Predicting Diabetes #

### Task ###
Predict the diabetes of Pima Indians, by using Naive Bayes and K-Nearest Neighbour Algorithms and compare these results to Weka.