import math

class NaiveBayes():
    # Initialisation
    def __init__(self):
        self.trainingData = []
        self.testingData = []

    # Main function of Naive Bayes Classifier
    def on(self):
        splitTrainData = self.splitClass(self.trainingData)
        classYes = splitTrainData['classYes']
        classNo = splitTrainData['classNo']
        # print ("Num total: {0}").format(len(self.trainingData))
        # print ("Num Yes: {0}").format(len(classYes))
        # print ("Num No: {0}").format(len(classNo))

        for testData in self.testingData:
            probYes = []
            probNo = []

            for attr in xrange(len(testData)):
                val = testData[attr] # Get row from set
                datasetY = []
                datasetN = []

                # Seperate each attribute into an individual set
                for i in xrange(len(classYes)):
                    datasetY.append(classYes[i][attr])
                for j in xrange(len(classNo)):
                    datasetN.append(classNo[j][attr])

                # Calculate using Probability density function
                densYes = self.probDensity(datasetY,val)
                densNo = self.probDensity(datasetN,val)
                # Store each Probability density result into its attribute
                probYes.append(densYes)
                probNo.append(densNo)
            # Classify based on the probabilies
            klass = self.classify(probYes,probNo,classYes, classNo)
            print klass
            # print("{0}. Class: {1}").format(count, klass) # For debugging

    # Classifies the test data using calculated probability densities
    def classify(self,probYes,probNo, classYes, classNo):
        resYes = float(len(classYes))/float(len(self.trainingData))
        resNo = float(len(classNo))/float(len(self.trainingData))

        for attr in xrange(len(probYes)):
            resYes *= probYes[attr]
        for attr in xrange(len(probNo)):
            resNo *= probNo[attr]

        # print("resYes: {0}").format(resYes)
        # print("resNo: {0}").format(resNo)
        if resNo > resYes:
            return 'no'
        elif resNo < resYes:
            return 'yes'
        elif resNo == resYes:
            return 'yes'

    # Calculate mean of data
    def mean(self,data):
        mean = math.fsum(data)/float(len(data))
        return mean

    # Calculate standard deviation of data
    def stdev(self,data,mean):
        stdev = math.sqrt(math.fsum([math.pow(d-mean,2) for d in data])/float(len(data)-1))
        return stdev

    # Calculate Probability Density of data
    def probDensity(self,data,x):
        mean = self.mean(data)
        stdev = self.stdev(data,mean)
        res = (1/(stdev*math.sqrt(2*math.pi))) * (math.exp(-(math.pow(x-mean,2)/(2*math.pow(stdev,2)))))
        return res

    # Seperate data into their classes: Yes and No
    def splitClass(self,dataset):
        classYes = []
        classNo = []
        for row in dataset:
            if row[-1] == 'yes':
                classYes.append(row)
            elif row[-1] == 'no':
                classNo.append(row)
        return {'classYes': classYes, 'classNo': classNo}
